package ru.megafon.formulaparser.enumeration;

public enum TokenType {
    CONSTANT(),
    VARIABLE(),
    PLUS(true, false),
    MINUS(true, false),
    MULTIPLY(true, true),
    DIVIDE(true, true),
    FUNCTION(true, true),
    LEFT_BRACKET(),
    RIGHT_BRACKET(),
    COMMA(),
    EOF();

    private boolean operand;
    private boolean priority;

    TokenType(boolean operand,
              boolean priority) {
        this.operand = operand;
        this.priority = priority;
    }

    TokenType() {
        this.operand = false;
        this.operand = false;
    }

    public boolean isOperand() {
        return operand;
    }

    public boolean isPriority() {
        return priority;
    }

    public boolean priorityIsEqualOrLess(boolean priority) {
        return (this.priority == priority || ! this.priority);
    }
}
