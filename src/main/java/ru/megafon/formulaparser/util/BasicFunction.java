package ru.megafon.formulaparser.util;

public class BasicFunction {

    private BasicFunction() {
    }

    public static final String PLUS_METHOD_NAME = "plus";
    public static final String MINUS_METHOD_NAME = "minus";
    public static final String MULTIPLY_METHOD_NAME = "multiply";
    public static final String DIVIDE_METHOD_NAME = "divide";

    public static float plus(float a, float b) {
        return a + b;
    }

    public static float minus(float a, float b) {
        return a - b;
    }

    public static float multiply(float a, float b) {
        return a * b;
    }

    public static float divide(float a, float b) {
        return a / b;
    }
}
