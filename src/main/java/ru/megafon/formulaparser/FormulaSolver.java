package ru.megafon.formulaparser;

import ru.megafon.formulaparser.dto.InputToken;
import ru.megafon.formulaparser.dto.OutputToken;
import ru.megafon.formulaparser.dto.OutputTokenConstant;
import ru.megafon.formulaparser.dto.OutputTokenFunction;
import ru.megafon.formulaparser.dto.OutputTokenVariable;
import ru.megafon.formulaparser.util.BasicFunction;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.Map;

import static ru.megafon.formulaparser.enumeration.TokenType.EOF;
import static ru.megafon.formulaparser.enumeration.TokenType.LEFT_BRACKET;

public class FormulaSolver {

    /**
     * Алгоритм Дейкстры - сортировочной станции
     * Входные параметры:
     * - formula - формула в виде строки
     */
    public List<OutputToken> prepareFormula(String formula) throws NoSuchMethodException, ClassNotFoundException {
        TokenParser tokenParser = new TokenParser(formula);
        List<OutputToken> formulaInRpn = new ArrayList<>();
        Deque<InputToken> deque = new ArrayDeque<>();
        InputToken inputToken;
        while (!(inputToken = tokenParser.nextSymbol()).getTokenType().equals(EOF)) {
            switch (inputToken.getTokenType()) {
                case CONSTANT:
                case VARIABLE:
                    formulaInRpn.add(mapOutputToken(inputToken));
                    break;
                case FUNCTION:
                    deque.push(inputToken);
                    break;
                case COMMA:
                    while (!LEFT_BRACKET.equals(deque.peek().getTokenType())) {
                        formulaInRpn.add(mapOutputToken(deque.pop()));
                    }
                    break;
                case PLUS:
                case MINUS:
                case MULTIPLY:
                case DIVIDE:
                    while (!deque.isEmpty() && deque.peek().getTokenType().isOperand()
                            && inputToken.getTokenType().priorityIsEqualOrLess(deque.peek().getTokenType().isPriority())) {
                        formulaInRpn.add(mapOutputToken(deque.pop()));
                    }
                    deque.push(inputToken);
                    break;
                case LEFT_BRACKET:
                    deque.push(inputToken);
                    break;
                case RIGHT_BRACKET:
                    while (!deque.isEmpty() && deque.peek().getTokenType() != LEFT_BRACKET) {
                        formulaInRpn.add(mapOutputToken(deque.pop()));
                    }
                    deque.pop();
                    break;
                default:
                    throw new RuntimeException("Ошибка, неправильная последовательнсоть токенов");
            }
        }
        while (!deque.isEmpty()) {
            formulaInRpn.add(mapOutputToken(deque.pop()));
        }
        return formulaInRpn;
    }

    /**
     * Алгоритм Лукасевич - обратной польской записи
     * Входные параметры:
     * - formula - набор токенов типа Константа, Переменная, Функция;
     * - variables - набор переменных со значениями.
     */
    public float solve(List<OutputToken> formula,
                       Map<String, Float> variables)
            throws InvocationTargetException, IllegalAccessException {
        Deque<Float> deque = new ArrayDeque<>();
        for (OutputToken token : formula) {
            switch (token.getTokenType()) {
                case CONSTANT:
                    deque.push(((OutputTokenConstant) token).getValue());
                    break;
                case VARIABLE:
                    deque.push(variables.get(((OutputTokenVariable) token).getVariableName()));
                    break;
                case FUNCTION:
                    Object[] arguments = new Object[((OutputTokenFunction) token).getMethod().getParameterCount()];
                    for (int i = ((OutputTokenFunction) token).getMethod().getParameterCount() - 1; i >= 0; i--) {
                        arguments[i] = castVariable(deque.pop(), ((OutputTokenFunction) token).getMethod()
                                .getParameterTypes()[i]);
                    }
                    deque.push(castReturnValue(((OutputTokenFunction) token).getMethod().invoke(null, arguments)));
                    break;
                default:
                    throw new RuntimeException("Тип токена:" + token.getTokenType().name() + " не поддерживается.");
            }
        }
        return deque.pop();
    }

    /**
     * Преобразование входных токенов в выходные, те получение значений для констант, получение методов из Reflection.API для
     * функций, преобразование базовых операций в функции.
     */
    private OutputToken mapOutputToken(InputToken token) throws NoSuchMethodException, ClassNotFoundException {
        switch (token.getTokenType()) {
            case CONSTANT:
                return new OutputTokenConstant(Float.parseFloat(token.getValue()));
            case VARIABLE:
                return new OutputTokenVariable(token.getValue());
            case PLUS:
                return new OutputTokenFunction(getBasicMethod(BasicFunction.PLUS_METHOD_NAME));
            case MINUS:
                return new OutputTokenFunction(getBasicMethod(BasicFunction.MINUS_METHOD_NAME));
            case MULTIPLY:
                return new OutputTokenFunction(getBasicMethod(BasicFunction.MULTIPLY_METHOD_NAME));
            case DIVIDE:
                return new OutputTokenFunction(getBasicMethod(BasicFunction.DIVIDE_METHOD_NAME));
            case FUNCTION:
                return new OutputTokenFunction(getAnyFunction(token.getValue()));
            default:
                throw new RuntimeException("Неверный тип токена");
        }
    }

    /**
     * Получение метода для базовой операции (+-*\) из Reflection.API
     */
    private Method getBasicMethod(String methodName) throws NoSuchMethodException {
        return BasicFunction.class.getMethod(methodName, float.class, float.class);
    }

    /**
     * Получение метода для функции java, используя Reflection.API, сначала будет получено название класса, затем осуществлен
     * поиск метода по имени
     */
    private Method getAnyFunction(String functionName) throws ClassNotFoundException {
        return Arrays.stream(Class.forName(getClassName(functionName)).getMethods())
                .filter(methodItem -> methodItem.getName().equals(getMethodName(functionName)))
                .findFirst().orElseThrow(() -> new RuntimeException("Метод не найден"));
    }

    /**
     * Строковое преобразование для получение полного имени класса
     */
    private static String getClassName(String functionName) {
        return functionName.substring(0, functionName.lastIndexOf('.'));
    }

    /**
     * Строковое преобразование для получение имени метода (функции)
     */
    private static String getMethodName(String functionName) {
        return functionName.substring(functionName.lastIndexOf('.') + 1);
    }

    /**
     * Приведение константы или переменной к типу, который поддерживается методом (функцией)
     */
    private static Object castVariable(float var,
                                       Class requiredType) {
        if (requiredType.equals(Float.class) || requiredType.equals(float.class)) {
            return var;
        }
        if (requiredType.equals(Integer.class) || requiredType.equals(int.class)) {
            return Math.round(var);
        }
        if (requiredType.equals(Double.class) || requiredType.equals(double.class)) {
            return ((Float) var).doubleValue();
        }
        throw new RuntimeException("Тип " + requiredType.getName() + " не поддерживается");
    }

    /**
     * Приведение возвращаемого значения функции к типу float
     */
    private static float castReturnValue(Object returnValue) {
        if (returnValue instanceof Float) {
            return (float) returnValue;
        }
        if (returnValue instanceof Double) {
            return ((Double) returnValue).floatValue();
        }
        if (returnValue instanceof Integer) {
            return ((Integer) returnValue).floatValue();
        }
        throw new RuntimeException("Тип " + returnValue.getClass().getName() + " не поддерживается");
    }
}