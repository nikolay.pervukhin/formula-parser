package ru.megafon.formulaparser.dto;

import ru.megafon.formulaparser.enumeration.TokenType;

public class InputToken {
    private TokenType tokenType;
    private String value;

    public InputToken(TokenType tokenType,
                      String value) {
        this.tokenType = tokenType;
        this.value = value;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "InputToken{" +
                "tokenType=" + tokenType +
                ", value='" + value + '\'' +
                '}';
    }
}
