package ru.megafon.formulaparser.dto;

import ru.megafon.formulaparser.enumeration.TokenType;

public class OutputTokenConstant extends OutputToken {
    private float value;

    public OutputTokenConstant(float value) {
        super.setTokenType(TokenType.CONSTANT);
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "OutputTokenConstant{" +
                "value=" + value +
                '}';
    }
}
