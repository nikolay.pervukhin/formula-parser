package ru.megafon.formulaparser.dto;

import ru.megafon.formulaparser.enumeration.TokenType;

import java.lang.reflect.Method;

public class OutputTokenFunction extends OutputToken {
    private Method method;

    public OutputTokenFunction(Method method) {
        super.setTokenType(TokenType.FUNCTION);
        this.method = method;
    }

    public Method getMethod() {
        return method;
    }

    @Override
    public String toString() {
        return "OutputTokenFunction{" +
                "method=" + method.getName() +
                '}';
    }
}
