package ru.megafon.formulaparser.dto;

import ru.megafon.formulaparser.enumeration.TokenType;

public class OutputToken {
    private TokenType tokenType;

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }
}
