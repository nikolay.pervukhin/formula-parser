package ru.megafon.formulaparser.dto;

import ru.megafon.formulaparser.enumeration.TokenType;

public class OutputTokenVariable extends OutputToken {
    private String variableName;

    public OutputTokenVariable(String variableName) {
        super.setTokenType(TokenType.VARIABLE);
        this.variableName = variableName;
    }

    public String getVariableName() {
        return variableName;
    }

    @Override
    public String toString() {
        return "OutputTokenVariable{" +
                "variableName='" + variableName + '\'' +
                '}';
    }
}
