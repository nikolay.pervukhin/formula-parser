package ru.megafon.formulaparser;

import ru.megafon.formulaparser.dto.OutputToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws Exception {

        FormulaSolver formulaSolver = new FormulaSolver();
        List<OutputToken> rpnFormula = formulaSolver.prepareFormula("((a+b)/(c-d))*((e+d)/(k-d)) * java.lang.Math.log10(m)");
        rpnFormula.forEach(System.out::println);

        Map<String, Float> data = new HashMap<>();
        data.put("a", 1.1f);
        data.put("b", 1.2f);
        data.put("c", 1.3f);
        data.put("d", 1.4f);
        data.put("e", 1.5f);
        data.put("k", 1.6f);
        data.put("m", 1.7f);
        System.out.printf("Набор 1, результат: %f%n", formulaSolver.solve(rpnFormula, data));
        System.out.printf("          проверка: %f%n", ((data.get("a")+data.get("b"))/(data.get("c")-data.get("d")))
                *((data.get("e")+data.get("d"))/(data.get("k")-data.get("d"))) * java.lang.Math.log10(data.get("m")));

        data = new HashMap<>();
        data.put("a", 2.1f);
        data.put("b", 2.2f);
        data.put("c", 2.3f);
        data.put("d", 2.4f);
        data.put("e", 2.5f);
        data.put("k", 2.6f);
        data.put("m", 2.7f);
        System.out.printf("Набор 2, результат: %f%n", formulaSolver.solve(rpnFormula, data));
        System.out.printf("          проверка: %f%n", ((data.get("a")+data.get("b"))/(data.get("c")-data.get("d")))
                *((data.get("e")+data.get("d"))/(data.get("k")-data.get("d"))) * java.lang.Math.log10(data.get("m")));

        data = new HashMap<>();
        data.put("a", 3.1f);
        data.put("b", 3.2f);
        data.put("c", 3.3f);
        data.put("d", 3.4f);
        data.put("e", 3.5f);
        data.put("k", 3.6f);
        data.put("m", 3.7f);
        System.out.println("Новая формула");
        rpnFormula = formulaSolver.prepareFormula("((a+b)/(c-d)) - java.lang.Math.pow(a, b) * ((e+d)/(k-d)) * java.lang.Math.log10(m)");
        rpnFormula.forEach(System.out::println);

        System.out.printf("Набор 3, результат: %f%n", formulaSolver.solve(rpnFormula, data));
        System.out.printf("          проверка: %f%n", ((data.get("a")+data.get("b"))/(data.get("c")-data.get("d")))
                - java.lang.Math.pow(data.get("a"), data.get("b")) * ((data.get("e")+data.get("d"))/(data.get("k")-data.get("d")))
                * java.lang.Math.log10(data.get("m")));

    }
}
