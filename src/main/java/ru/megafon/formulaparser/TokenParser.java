package ru.megafon.formulaparser;

import ru.megafon.formulaparser.dto.InputToken;
import ru.megafon.formulaparser.enumeration.TokenType;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;

import static ru.megafon.formulaparser.enumeration.TokenType.*;

public class TokenParser {

    private StreamTokenizer input;

    private static final String CONSTANTS = "[0-9]*\\.?[0-9]*";
    private static final String FUNCTIONS = "[a-zA-z]([a-zA-z.]|[0-9])*";
    private static final String VARIABLES = "[a-z]";

    public TokenParser(String textFormula) {
        Reader r = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(textFormula.getBytes())));
        input = new StreamTokenizer(r);
        input.resetSyntax();
        input.eolIsSignificant(true);
        input.wordChars('a', 'z');
        input.wordChars('A', 'Z');
        input.wordChars('0', '9');
        input.wordChars('.', '.');
        input.whitespaceChars('\u0000', ' ');
        input.whitespaceChars('\n', '\t');
    }

    public InputToken nextSymbol() {
        try {
            switch (input.nextToken()) {
                case StreamTokenizer.TT_EOL:
                    return new InputToken(TokenType.EOF, input.sval);
                case StreamTokenizer.TT_EOF:
                    return new InputToken(TokenType.EOF, input.sval);
                case StreamTokenizer.TT_WORD: {
                    if (input.sval.matches(VARIABLES)) {
                        return new InputToken(TokenType.VARIABLE, input.sval);
                    } else if (input.sval.matches(CONSTANTS)) {
                        return new InputToken(TokenType.CONSTANT, input.sval);
                    } else if (input.sval.matches(FUNCTIONS)) {
                        return new InputToken(TokenType.FUNCTION, input.sval);
                    } else {
                        throw new RuntimeException("Токен не распознан");
                    }
                }
                default:
                    return new InputToken(mapOperand(input.ttype), null);
            }
        } catch (IOException e) {
            return new InputToken(TokenType.EOF, null);
        }
    }

    private static TokenType mapOperand(int value) {
        switch (Character.toString((char) value)) {
            case "+":
                return TokenType.PLUS;
            case "-":
                return TokenType.MINUS;
            case "*":
                return TokenType.MULTIPLY;
            case "/":
                return TokenType.DIVIDE;
            case "(":
                return TokenType.LEFT_BRACKET;
            case ")":
                return TokenType.RIGHT_BRACKET;
            case ",":
                return TokenType.COMMA;
            default:
                throw new RuntimeException("Токен не распознан");
        }
    }
}