# Formula-parser
Подсчет наборов данных типа float по математической формуле, представленной в виде текстовой строки. 

Так как решение предполагает обработку больших наборов записей (1000+), при этом формула меняется реже, то выражение сначала преобразовывается в обратную польскую запись (RPN), на этом этапе строка разбивается на токены. Далее означиваются константы и находятся все функции через Reflection.API, те осуществляются все ресурсоемкие операции.
Потом, используя данный набор, можно быстро обрабатывать формулу просто подставляя значения переменных. Для решения задачи используются два фундаментальных алгоритма: Алгоритм Дейкстры (Сортировочная станция) для разбора формулы и преобразование ее в обратную польскую запись и сам алгоритм обработки польской записи со стеком. 

## Допущения
Для упрощения задачи были сделаны некоторые допущения:
Все операции выполняются с типом данных float. Это влияет на точность, но экономит ресурсы. В случае необходимости можно перейти на тип Double.
Названия переменных пишуться 1 буквой для упрощения парсера.
Названия функций пишется по полному пути класса, например java.lang.Math.sin. В случае необходимости можно сделать некоторые значения по-умолчанию. 

## Программа
Основной класс FormulaSolver содержит два метода:

prepareFormula - преобразование строки в List токенов, которые представляют из себя токены трех видов: Константа (со значением float), Переменная (с названием переменной) и Функция (с методом из Reflection.API). Последовательность токенов определена в обратной польской записи.
Пример:
OutputTokenVariable{variableName='a'}
OutputTokenVariable{variableName='b'}
OutputTokenFunction{method=plus}
OutputTokenVariable{variableName='c'}
OutputTokenVariable{variableName='d'}
OutputTokenFunction{method=minus}
OutputTokenFunction{method=divide}
OutputTokenVariable{variableName='e'}
OutputTokenVariable{variableName='d'}
OutputTokenFunction{method=plus}
OutputTokenVariable{variableName='k'}
OutputTokenVariable{variableName='d'}
OutputTokenFunction{method=minus}
OutputTokenFunction{method=divide}
OutputTokenFunction{method=multiply}
OutputTokenVariable{variableName='m'}
OutputTokenFunction{method=log10}
OutputTokenFunction{method=multiply}

Для разбития строки на токены, используется класс TokenParser. Он помогает из входящего потока преобразовать его в поток токенов по заданным разделителям. 

solve - по заданному List’у из токенов решает формулу, означивая переменные и вталкивая их в стек и выталкивая подставляя их в функции в качестве аргументов.
